#include <unistd.h>
#include <stdio.h>
#include <signal.h>


#include "canfestival.h"
#include "TestMaster.h"
#include "can_driver.h"
#include "timers_driver.h"
#include "rcan.h"
#include "menu.h"

static uint8_t slavenodeid = 0x00;

/*****************************************************************************/
void TestMaster_heartbeatError(CO_Data *d, UNS8 heartbeatID) {
    (void) d;
    MSG("TestMaster_heartbeatError %d\n", heartbeatID);
}

/********************************************************
 * ConfigureSlaveNode is responsible to
 *  - setup master RPDO 1 to receive TPDO 1 from id 0x40
 *  - setup master TPDO 1 to send RPDO 1 to id 0x40
 ********************************************************/
void TestMaster_initialisation(CO_Data *d) {
    (void) d;
    UNS32 PDO1_COBID = 0x0180 + slavenodeid;
    UNS32 PDO2_COBID = 0x0200 + slavenodeid;
    UNS32 size = sizeof(UNS32);

    MSG("TestMaster_initialisation\n");
    masterSendNMTstateChange(&ObjDict_Data, (UNS8) slavenodeid, NMT_Start_Node);
//    /*****************************************
//     * Define RPDOs to match slave ID=0x40 TPDOs*
//     *****************************************/
//    writeLocalDict(&ObjDict_Data, /*CO_Data* d*/
//                   0x1400, /*UNS16 index*/
//                   0x01, /*UNS8 subind*/
//                   &PDO1_COBID, /*void * pSourceData,*/
//                   &size, /* UNS8 * pExpectedSize*/
//                   RW);  /* UNS8 checkAccess */
//
//
//    /*****************************************
//     * Define TPDOs to match slave ID=0x40 RPDOs*
//     *****************************************/
//    writeLocalDict(&ObjDict_Data, /*CO_Data* d*/
//                   0x1800, /*UNS16 index*/
//                   0x01, /*UNS8 subind*/
//                   &PDO2_COBID, /*void * pSourceData,*/
//                   &size, /* UNS8 * pExpectedSize*/
//                   RW);  /* UNS8 checkAccess */
}

static int init_step = 0;

/*Froward declaration*/
static void ConfigureSlaveNode(CO_Data *d, UNS8 nodeId);

/**/
static void CheckSDOAndContinue(CO_Data *d, UNS8 nodeId) {
    UNS32 abortCode;

    if (getWriteResultNetworkDict(d, nodeId, &abortCode) != SDO_FINISHED)
            MSG("Master : Failed in initializing slave %2.2x, step %d, AbortCode :%4.4x \n", nodeId, init_step,
               abortCode);

    /* Finalise last SDO transfer with this node */
    closeSDOtransfer(&ObjDict_Data, nodeId, SDO_CLIENT);

    ConfigureSlaveNode(d, nodeId);
}

/********************************************************
 * ConfigureSlaveNode is responsible to
 *  - setup slave TPDO 1 transmit time
 *  - setup slave TPDO 2 transmit time
 *  - setup slave Heartbeat Producer time
 *  - switch to operational mode
 *  - send NMT to slave
 ********************************************************
 * This an example of :
 * Network Dictionary Access (SDO) with Callback
 * Slave node state change request (NMT)
 ********************************************************
 * This is called first by TestMaster_preOperational
 * then it called again each time a SDO exchange is
 * finished.
 ********************************************************/
static void ConfigureSlaveNode(CO_Data *d, UNS8 nodeId) {
    UNS8 res;
    MSG("Master : ConfigureSlaveNode %2.2x\n", nodeId);
    MSG("nodeid slave=%x\n", nodeId);
    switch (++init_step) {
        case 1: {    /*disable Slave's TPDO 1 */
            UNS32 TPDO_COBId = 0x80000180 + nodeId;

            MSG("Master : disable slave %2.2x TPDO 1 \n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1800, /*UNS16 index*/
                                           0x01, /*UNS8 subindex*/
                                           4, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &TPDO_COBId,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 2: {    /*setup Slave's TPDO 1 to be transmitted on SYNC*/
            UNS8 Transmission_Type = 0x01;

            MSG("Master : set slave %2.2x TPDO 1 transmit type\n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1800, /*UNS16 index*/
                                           0x02, /*UNS8 subindex*/
                                           1, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &Transmission_Type,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 3: {    /*re-enable Slave's TPDO 1 */
            UNS32 TPDO_COBId = 0x00000180 + nodeId;

            MSG("Master : re-enable slave %2.2x TPDO 1\n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1800, /*UNS16 index*/
                                           0x01, /*UNS8 subindex*/
                                           4, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &TPDO_COBId,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 4: {    /*disable Slave's TPDO 2 */
            UNS32 TPDO_COBId = 0x80000200 + nodeId;

            MSG("Master : disable slave %2.2x RPDO 1\n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1400, /*UNS16 index*/
                                           0x01, /*UNS8 subindex*/
                                           4, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &TPDO_COBId,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;


        case 5: {
            UNS8 Transmission_Type = 0x01;

            MSG("Master : set slave %2.2x RPDO 1 receive type\n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1400, /*UNS16 index*/
                                           0x02, /*UNS8 subindex*/
                                           1, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &Transmission_Type,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 6: {    /*re-enable Slave's TPDO 1 */
            UNS32 TPDO_COBId = 0x00000200 + nodeId;

            MSG("Master : re-enable %2.2x RPDO 1\n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1400, /*UNS16 index*/
                                           0x01, /*UNS8 subindex*/
                                           4, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &TPDO_COBId,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 7: {
            UNS16 Heartbeat_Producer_Time = 0x03E8;
            MSG("Master : set slave %2.2x heartbeat producer time \n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1017, /*UNS16 index*/
                                           0x00, /*UNS8 subindex*/
                                           2, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &Heartbeat_Producer_Time,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 8: {    /*disable Slave's TPDO 2 */
            UNS32 TPDO_COBId = 0x80000280 + nodeId;

            MSG("Master : disable slave %2.2x TPDO 2 \n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1801, /*UNS16 index*/
                                           0x01, /*UNS8 subindex*/
                                           4, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &TPDO_COBId,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 9: {    /*disable Slave's TPDO 3 */
            UNS32 TPDO_COBId = 0x80000380 + nodeId;

            MSG("Master : disable slave %2.2x TPDO 3 \n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1802, /*UNS16 index*/
                                           0x01, /*UNS8 subindex*/
                                           4, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &TPDO_COBId,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 10: {    /*disable Slave's TPDO 2 */
            UNS32 TPDO_COBId = 0x80000480 + nodeId;

            MSG("Master : disable slave %2.2x TPDO 4 \n", nodeId);
            res = writeNetworkDictCallBack(d, /*CO_Data* d*/
                    /**TestSlave_Data.bDeviceNodeId, UNS8 nodeId*/
                                           nodeId, /*UNS8 nodeId*/
                                           0x1803, /*UNS16 index*/
                                           0x01, /*UNS8 subindex*/
                                           4, /*UNS8 count*/
                                           0, /*UNS8 dataType*/
                                           &TPDO_COBId,/*void *data*/
                                           CheckSDOAndContinue, /*SDOCallback_t Callback*/
                                           0); /* use block mode */
        }
            break;

        case 11:
            /* Put the master in operational mode */
            setState(d, Operational);

            /* Ask slave node to go in operational mode */
            masterSendNMTstateChange(d, nodeId, NMT_Start_Node);
    }

}

void TestMaster_preOperational(CO_Data *d) {
    (void) d;
    MSG("TestMaster_preOperational\n");
    //ConfigureSlaveNode(&ObjDict_Data, (UNS8) slavenodeid);

}

void TestMaster_operational(CO_Data *d) {
    (void) d;
    MSG("TestMaster_operational\n");

}

void TestMaster_stopped(CO_Data *d) {
    (void) d;
    MSG("TestMaster_stopped\n");
}

void TestMaster_post_sync(CO_Data *d) {

//    puts("\033[?25l");
//    MSG("\033[H");
    (void) d;

    //MSG("MicroMod Digital Out: %2.2x\n", Read_Inputs_8_Bit[0]);
    //MSG("MicroMod Digital Out: %2.2x\n", Write_Outputs_8_Bit[0]);


//    puts("\033[0J");
//    puts("\033[?25h");
}

void TestMaster_post_TPDO(CO_Data *d) {
    (void) d;
    MSG("TestMaster_post_TPDO\n");
}


#if !defined(WIN32) || defined(__CYGWIN__)

void catch_signal(int sig) {
    signal(SIGTERM, catch_signal);
    signal(SIGINT, catch_signal);

    MSG("Got Signal %d\n", sig);
}


#endif

void help(void) {
    MSG("**************************************************************\n");
    MSG("*  TestMasterMicroMod                                        *\n");
    MSG("*                                                            *\n");
    MSG("*  A simple example for PC.                                  *\n");
    MSG("*  A CanOpen master that control a MicroMod module:          *\n");
    MSG("*  - setup module TPDO 1 transmit type                       *\n");
    MSG("*  - setup module RPDO 1 transmit type                       *\n");
    MSG("*  - setup module hearbeatbeat period                        *\n");
    MSG("*  - disable others TPDOs                                    *\n");
    MSG("*  - set state to operational                                *\n");
    MSG("*  - send periodic SYNC                                      *\n");
    MSG("*  - send periodic RPDO 1 to Micromod (digital output)       *\n");
    MSG("*  - listen Micromod's TPDO 1 (digital input)                *\n");
    MSG("*  - Mapping RPDO 1 bit per bit (digital input)              *\n");
    MSG("*                                                            *\n");
    MSG("*   Usage:                                                   *\n");
    MSG("*   ./TestMasterMicroMod  [OPTIONS]                          *\n");
    MSG("*                                                            *\n");
    MSG("*   OPTIONS:                                                 *\n");
    MSG("*     -l : Can library [\"libcanfestival_can_virtual.so\"]     *\n");
    MSG("*                                                            *\n");
    MSG("*    Slave:                                                  *\n");
    MSG("*     -i : Slave Node id format [0x01 , 0x7F]                *\n");
    MSG("*                                                            *\n");
    MSG("*    Master:                                                 *\n");
    MSG("*     -m : bus name [\"1\"]                                    *\n");
    MSG("*     -M : 1M,500K,250K,125K,100K,50K,20K,10K                *\n");
    MSG("*                                                            *\n");
    MSG("**************************************************************\n");
}

/***************************  INIT  *****************************************/
void InitNodes(CO_Data *d, UNS32 id) {
    (void) d;
    (void) id;
    /****************************** INITIALISATION MASTER *******************************/

    /* Defining the node Id */
    unsigned char nodeID = 0x1;
    setNodeId(&ObjDict_Data, nodeID);
    setState(&ObjDict_Data, Initialisation);

    /* init */


}

/***************************  EXIT  *****************************************/
void Exit(CO_Data *d, UNS32 id) {
    (void) d;
    (void) id;
    masterSendNMTstateChange(&ObjDict_Data, 0x02, NMT_Reset_Node);

    //Stop master
    setState(&ObjDict_Data, Stopped);
}

/****************************************************************************/
/***************************  MAIN  *****************************************/
/****************************************************************************/
int main(int argc, char **argv) {
    ObjDict_Data.heartbeatError = TestMaster_heartbeatError;
    ObjDict_Data.initialisation = TestMaster_initialisation;
    ObjDict_Data.preOperational = TestMaster_preOperational;
    ObjDict_Data.operational = TestMaster_operational;
    ObjDict_Data.stopped = TestMaster_stopped;
    ObjDict_Data.post_sync = TestMaster_post_sync;
    ObjDict_Data.post_TPDO = TestMaster_post_TPDO;

    //Init timer
    TimerInit();

    //Canopen
    if(canOpen(TUI_CHOSECAN(), TUI_CHOISESPEED(), &ObjDict_Data) != 0){
        MSG("can not start\r\n");
    }
    //if (!fork()) {
//дочерний процесс
#if defined(RCAN_UNIX)
system("/home/pfirsov/projects/bushs/vcan.sh > /dev/null 2>&1");
#endif
    //}
//основной процесс

    //getc(stdin);
    // Start timer thread
    StartTimerLoop(&InitNodes);



    TUI_INITWIN();


#ifdef TUI
    int n_win = 0;
    int connect = 1;
    while(connect) {
        remote_NMT_t remote_NMT;
        switch (n_win){
            case 0: {
                n_win = tui_NMT_choiser(&remote_NMT);
                if(n_win)
                    break;
                }



                if(!remote_NMT.state){
                    switch (remote_NMT.send_set) {
                        case 0:remote_NMT.send_set = Initialisation;break;
                        case 1:remote_NMT.send_set = Pre_operational;break;
                        case 2:remote_NMT.send_set = Operational;break;
                        case 3:remote_NMT.send_set = Stopped;break;
                    }
                    setState(&ObjDict_Data, remote_NMT.send_set);
                }
                else{
                    switch (remote_NMT.send_set) {
                        case 0:remote_NMT.send_set = NMT_Start_Node;break;
                        case 1:remote_NMT.send_set = NMT_Stop_Node;break;
                        case 2:remote_NMT.send_set = NMT_Enter_PreOperational;break;
                        case 3:remote_NMT.send_set = NMT_Reset_Node;break;
                        case 4:remote_NMT.send_set = NMT_Reset_Comunication;break;
                    }
                    masterSendNMTstateChange(&ObjDict_Data, 0, remote_NMT.send_set);
                }
                //MSG_TIME("ds %d", remote_NMT.send_set);
                break;
            case 1:
                    n_win = tui_viewer_log();
                break;
            case 2:
                connect = 0;
                break;
        }
    }
    //TUI_DELWINDOWS();
#else
    // wait Ctrl-C
    pause();
#endif
    MSG("Finishing.\n");
    // Reset the slave node for next use (will stop emitting heartbeat)
    masterSendNMTstateChange(&ObjDict_Data, (UNS8) slavenodeid, NMT_Reset_Node);

    // Stop master
    setState(&ObjDict_Data, Stopped);

    // Stop timer thread
    StopTimerLoop(&Exit);

    //stop Can
    canClose(&ObjDict_Data);
#ifdef TUI
    TUI_DELWINDOWS();
#endif
    return 0;
}


