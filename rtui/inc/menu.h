//
// Created by Paul on 28.12.2021.
//

#ifndef DS401_CLI_MASTER_MENU_H
#define DS401_CLI_MASTER_MENU_H
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>

typedef struct {
    uint8_t send_set;   // 0 - set, 1 - send
    uint8_t nodeID;     // If changed send NMT
    uint8_t state;
}remote_NMT_t;

#define SIZEARR(arr) sizeof(arr)/sizeof(*arr)

void tui_insert_log(int num, char *str, int val);
int tui_viewer_log(void);

void tui_RPDO(int RPDOn, int numMap, const char *str, ...);

void tui_updateNMTable(const int *NMTable);
void tui_NMTstate(int state);
uint8_t tui_NMT_choiser(remote_NMT_t *remote_NMT);


void tui_initWindows(void);
void tui_delWindows(void);

int tui_choiseCAN(void);
int tui_choiseSpeed(void);


#endif //DS401_CLI_MASTER_MENU_H
