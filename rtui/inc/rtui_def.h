//
// Created by Paul on 28.12.2021.
//

#ifndef DS401_CLI_MASTER_DEFS_H
#define DS401_CLI_MASTER_DEFS_H

#include "rcan_def.h"
#include <ncurses.h>


#if defined(RCAN_UNIX)
#define CAN_SIZE 12
const int can_faces[CAN_SIZE] = {
        SOCKET_VCAN0, SOCKET_VCAN1, SOCKET_VCAN2,
        SOCKET_CAN0,  SOCKET_CAN1,  SOCKET_CAN2,
        PCAN_USBBUS1, PCAN_USBBUS2, PCAN_USBBUS3,
        PCAN_PCIBUS1, PCAN_PCIBUS2, PCAN_PCIBUS3
};

const char can_labels[CAN_SIZE][20] = {
        "SOCKET_VCAN0", "SOCKET_VCAN1", "SOCKET_VCAN2",
        "SOCKET_CAN0",  "SOCKET_CAN1",  "SOCKET_CAN2",
        "PCAN_USBBUS1", "PCAN_USBBUS2", "PCAN_USBBUS3",
        "PCAN_PCIBUS1", "PCAN_PCIBUS2", "PCAN_PCIBUS3"
};
#elif defined(RCAN_MACOS)
#define CAN_SIZE 3
const int can_faces[CAN_SIZE] = {
        PCAN_USBBUS1, PCAN_USBBUS2, PCAN_USBBUS3
};

const char can_labels[CAN_SIZE][20] = {
        "PCAN_USBBUS1", "PCAN_USBBUS2", "PCAN_USBBUS3"
};
#endif

#define SPEED_SIZE 14
const int speed_faces[SPEED_SIZE] = {
        PCAN_BAUD_1M,
        PCAN_BAUD_800K,
        PCAN_BAUD_500K,
        PCAN_BAUD_250K,
        PCAN_BAUD_125K,
        PCAN_BAUD_100K,
        PCAN_BAUD_95K,
        PCAN_BAUD_83K,
        PCAN_BAUD_50K,
        PCAN_BAUD_47K,
        PCAN_BAUD_33K,
        PCAN_BAUD_20K,
        PCAN_BAUD_10K,
        PCAN_BAUD_5K
};

const char speed_labels[SPEED_SIZE][20] = {
        "PCAN_BAUD_1M",
        "PCAN_BAUD_800K",
        "PCAN_BAUD_500K",
        "PCAN_BAUD_250K",
        "PCAN_BAUD_125K",
        "PCAN_BAUD_100K",
        "PCAN_BAUD_95K",
        "PCAN_BAUD_83K",
        "PCAN_BAUD_50K",
        "PCAN_BAUD_47K",
        "PCAN_BAUD_33K",
        "PCAN_BAUD_20K",
        "PCAN_BAUD_10K",
        "PCAN_BAUD_5K"
};


#endif //DS401_CLI_MASTER_DEFS_H
