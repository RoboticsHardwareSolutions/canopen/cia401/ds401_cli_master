//
// Created by Paul on 05.01.2022.
//

#ifndef DS401_CLI_MASTER_LOGS_H
#define DS401_CLI_MASTER_LOGS_H
#include <ncurses.h>

void init_log(WINDOW *win, int height, int width, int yLog, int xLog, char *nameLog);
void _insert_log(char *str);
int _viewer_log(void);
void _dest_page(void);
void _delwin(void);


#endif //DS401_CLI_MASTER_LOGS_H
