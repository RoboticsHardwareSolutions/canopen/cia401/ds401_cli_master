if (MSVC OR MSYS OR MINGW)
    add_definitions(-DGUI_FOR_WINDOWS)
endif ()

if (APPLE)
    add_definitions(-DGUI_FOR_APPLE)
endif ()

if (UNIX AND NOT APPLE)
    add_definitions(-DGUI_FOR_UNIX)
endif ()


set(RTUI_DIRECTORIES  rtui/inc rtui/src rtui/ )
file(GLOB_RECURSE RTUI_EXECUTABLE_FILES  "rtui/inc/*.*" "rtui/src/*.*" )