//
// Created by Paul on 04.01.2022.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "logs.h"

#define PAGE_SIZE 500 /* Default number of lines */
#define WIN_SIZE 7 //(LINES - 2) /* Size of window, making room for bottom prompt */
#define LINE_SIZE 70 /* Max characters in a line. Expandable */
#define TAB_WIDTH 4

typedef struct
{
    char *line;
    int size; // size of array, not string
} LINE;

typedef struct
{
    LINE *text; // lines of text
    int numlines;
    int size; // size of array
} PAGE;





PAGE page;
WINDOW *logs;
int beg = 0;
int end = WIN_SIZE;
int y, x; // position on screen
int i;
int y_offset = 0; // TODO: move to local scope
int tab_offset = 0;


int yMax, xMax;

void init_line(LINE *s)
{
    s->size = LINE_SIZE;
    s->line = (char *)malloc(LINE_SIZE * sizeof(char));
    s->line[0] = '\0';
} // init_line


void init_page(PAGE *p,  int size)
{
    p->text = (LINE *)malloc(size * sizeof(LINE));

    int i;
    for(i = 0; i < size; i++)
    {
        init_line(p->text + i);
    }
    p->numlines = 0;
    p->size = size;
} // init_page


void print_loc(int x, int y)
{
    //int oldx, oldy;
    //getyx(logs, oldy, oldx);
    mvwprintw(logs, 0, xMax - 10 - 25, " x: %d y: %d o: %d  ", x, y, y_offset);
    //wmove(logs, oldy, oldx);
    wrefresh(logs);
}


// NOTE: This moves the cursor to the end of the displayed text
void print_page(const PAGE *p, int start, int end)
{
    int i, line;
    for(i = start, line = 0; i < p->numlines && i < end; i++, line++)
    {
        wmove(logs, line + 1, 1);
        wclrtoeol(logs);
        //wprintw(logs, " %d", i);
        wprintw(logs, " %s", p->text[i].line);
    }
    if(start < end)
    {
        wmove(logs, line + 1, 1);
        wclrtoeol(logs); // if we deleted a line this may be necessary
        wmove(logs, line - 1, 1);
    }

    box(logs, 0, 0);
    mvwprintw(logs, 0, 1, "%s", "logs");
    wrefresh(logs);

} // print_page


void expand_page(PAGE *p)
{
    int newsize = p->size * 2;
    LINE *newline = malloc(newsize * sizeof(LINE));

    int i;
    for(i = 0; i < p->size; i++) // copy old lines
        newline[i] = p->text[i];
    for(i = p->size; i < newsize; i++) // init new lines
        init_line(newline + i);

    p->text = newline;
    p->size = newsize;
} // expand_page

// WARNING: Expansion function implemented but not tested
void insert_line(PAGE *p, int index, char * str)
{
    if( p->numlines >= p->size ) expand_page(p);

    LINE newline;
    init_line(&newline);
    strcpy(newline.line, str);

    int i;

    for(i = p->numlines - 1; i >= index; i--)
        p->text[i + 1] = p->text[i];

    p->text[index] = newline;
    (p->numlines)++;
} // insert_line

void move_up(PAGE *p, int *x, int *y)
{
    if( *y > 1 )
    {
        --(*y);
    }
    else if (y_offset > 0)
    {
        --(y_offset);
        print_page(p, 1 + y_offset, WIN_SIZE + y_offset);
    }
    if( *x > strlen(p->text[*y + y_offset].line) + 1 ) // cursor adjusts
        *x = strlen(p->text[*y + y_offset].line) + 1;  // to smaller lines
    wmove(logs, *y, *x);
}

void move_down(PAGE *p, int *x, int *y)
{
    if( *y < WIN_SIZE - 1 && *y < p->numlines  - 1 )
    {
        ++(*y);
    }
    else if ( *y + y_offset < p->numlines - 1 )
    {
        ++(y_offset);
        print_page(p, 1 + y_offset, WIN_SIZE + y_offset);
    }

    if( *x > strlen(p->text[*y + y_offset].line) + 1 )
        *x = strlen(p->text[*y + y_offset].line) + 1;
    wmove(logs, *y, *x);
}


void remove_line(PAGE *p, int index)
{
    if( p->numlines > 1 )
    {
        free(p->text[index].line);

        int i;
        for(i = index; i < p->numlines - 1; i++)
        {
            p->text[i] = p->text[i + 1];
        }
        (p->numlines)--;
    }
} // remove_line




void init_log(WINDOW *win, int height, int width, int yLog, int xLog, char *nameLog){

    init_page(&page, PAGE_SIZE);
    page.numlines = 1;

    logs = win;


    getmaxyx(stdscr, yMax, xMax);//TODO delete
    logs = newwin(height, width, yLog, xLog);
    box(logs, 0, 0);
    refresh();
    mvwprintw(logs, 0, 1, "%s", nameLog);
    wrefresh(logs);
    keypad(logs, true);

    getyx(logs, y, x);
}


void _insert_log(char *str){
    beg = y_offset;
    end = WIN_SIZE + y_offset;
    insert_line(&page, y + y_offset, str);
    print_page(&page, beg, end);
    move_down(&page, &x, &y);
    print_loc(x, y);
}



void dest_page(PAGE *p){

    int i;
    for(i = 0; i < p->numlines; i++)
    {
        free(p->text[i].line); // maybe replace with dest_line()
    }
    free(p->text);
}
void _delwin(void){
    delwin(logs);
    endwin();
}
void _dest_page(void){
    dest_page(&page);
}

int _viewer_log(void)
{
    while(1)
    {
        print_loc(x, y);
        beg = 0 + y_offset;
        end = WIN_SIZE + y_offset;
        int ch = wgetch(logs);



        switch(ch) {
            case KEY_UP:
                move_up(&page, &x, &y);
                break;
            case KEY_DOWN:
                move_down(&page, &x, &y);
                break;
            case 'w':
                return 0;
            case 'q':
                return 2;
//            case '\n': // newline
//                insert_line(&page, y + y_offset + 1);
//                print_page(&page, beg, end);
//                move_down(&page, &x, &y);
//                break;
//            default: // all other chars
//                insert_char(&page.text[y + y_offset], ch, x - 1);
//                print_page(&page, beg, end);
//                move_right(&page, &x, &y);
        }
    }
    /* end curses */
    return 0;
} // main