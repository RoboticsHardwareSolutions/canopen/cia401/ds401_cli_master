//
// Created by Paul on 28.12.2021.
//
#include "menu.h"
#include "logs.h"
#include "rtui_def.h"

int menu_choiser(const int *faces, const char labels[][20], const int size);


WINDOW *win_NMTstateOfSlaves, *win_NMT, *win_RPDO, *win_logs;



void vmvwprintw(WINDOW *win, int y, int x, const char *str, va_list args){

    wmove(win, y, x);
    vwprintw(win, str, args);
}


void tui_RPDO(int RPDOn, int numMap, const char *str, ...){
    va_list args;
    va_start(args, str);
    mvwprintw(win_RPDO, 1,             22*RPDOn + 1, "RPDO %d", RPDOn);
    vmvwprintw(win_RPDO, 2 + numMap,    22*RPDOn + 1, str, args);
    va_end(args);
    wrefresh(win_RPDO);
}

/**************************************************************************/
/* Window of state and control NMT                                        */
/**************************************************************************/

void tui_NMTstate(int state){
    mvwprintw(win_NMT, 1, 1, "My state is: 0x%02x", state);
    wrefresh(win_NMT);
}


void tui_updateNMTable(const int *NMTable){
    wclear(win_NMTstateOfSlaves);
    box(win_NMTstateOfSlaves, 0, 0);
    mvwprintw(win_NMTstateOfSlaves, 0, 1, "NMT state of slaves");

    int cntOfSlaves = 0;
    for (int i = 0 ; i < 128 ; i++) {
        if(NMTable[i] != 0x0F){
            cntOfSlaves++;
            mvwprintw(win_NMTstateOfSlaves, cntOfSlaves, 1, "nodeId: %d state: %d", i, NMTable[i]);
        }
    }
    wrefresh(win_NMTstateOfSlaves);
}

uint8_t tui_NMT_choiser(remote_NMT_t *remote_NMT){

#define SIZE_M 4
#define SIZE_S 5
    const char labels_myNMT[SIZE_M][20] = {"Initialisation", "Pre_operational", "Operational", "Stopped"};
    const char labels_sendNMT[SIZE_S][20] = {"Start_Node", "Stop_Node", "PreOperational", "Reset_Node", "Reset_Comunication"};

    int choice;
    static uint8_t highlight = 0;
    static uint8_t col = 0;
    static uint8_t size = SIZE_M;

    while (1) {
        for (int i = 0; i < SIZE_M; i++) {
            if ((i == highlight)&&!col)
                wattron(win_NMT, A_REVERSE);
            mvwprintw(win_NMT, i + 2, 1, labels_myNMT[i]);
            wattroff(win_NMT, A_REVERSE);
        }

        for (int i = 0; i < SIZE_S; i++) {
            if ((i == highlight)&&col)
                wattron(win_NMT, A_REVERSE);
            mvwprintw(win_NMT, i + 2, 25, labels_sendNMT[i]);
            wattroff(win_NMT, A_REVERSE);
        }
        choice = wgetch(win_NMT);
        switch (choice) {
            case KEY_UP:
                highlight = (!highlight) ? 0 : --highlight;
                break;

            case KEY_DOWN:
                highlight = (highlight == (col?SIZE_S:SIZE_M) - 1) ? (col?SIZE_S:SIZE_M) - 1 : ++highlight;
                break;
            case KEY_LEFT:
                if (col)
                    col = !col;
                if(highlight > SIZE_M - (SIZE_S - SIZE_M))
                    highlight = SIZE_M - (SIZE_S - SIZE_M);
                break;
            case KEY_RIGHT:
                if (!col)
                    col = !col;
                break;
            case 'w':
                return 1;
                break;
            case 'q':
                return 2;
                break;
            default:
                break;
        }
        if (choice == 10)
            break;
    }
    remote_NMT->send_set = highlight;
    remote_NMT->nodeID = 0;
    remote_NMT->state = col;
    return 0;
}




/**************************************************************************/
/* Window of logs                                                         */
/**************************************************************************/
int flag_inViewerLog = 0;

void tui_insert_log(int num, char *str, int val){
    if(!flag_inViewerLog){
        char res[70];
        sprintf(res,"0X%x %s 0X%x",num, str, val);
        _insert_log(res);
    }

}



int tui_viewer_log(void){
    flag_inViewerLog = 1;
    int res = _viewer_log();
    flag_inViewerLog = 0;

    return res;
}



/**************************************************************************/
/* Init all windows after configuration can                               */
/**************************************************************************/


void createWin(WINDOW *win, char *name){
    box(win, 0, 0);
    refresh();
    mvwprintw(win, 0, 1, "%s", name);
    wrefresh(win);
    keypad(win, true);
}

void createWinRPDO(void){
    win_RPDO = newwin(10,50,11,5);
    createWin(win_RPDO, "RPDO");
}

void tui_initWindows(void){
    int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);

    win_NMTstateOfSlaves = newwin(10,25,1,5);
    createWin(win_NMTstateOfSlaves, "NMT state of slaves");

    win_NMT = newwin(10,50,1,30);
    createWin(win_NMT, "NMT master");

    createWinRPDO();

//    win_logs = newwin(7,xMax - 10,21,5);
//    createWin(win_logs, "Logs");
//    mvwprintw(win_logs , 2, 1, "alkhdgf");
//    wrefresh(win_logs);
    init_log(win_logs, 7,xMax - 10,21,5, "Logs");

}


void tui_delWindows(void){
    delwin( win_NMTstateOfSlaves );
    endwin();
    delwin( win_NMT );
    endwin();
    delwin( win_RPDO );
    endwin();
    //delwin( win_logs );
    _delwin();
    _dest_page();

}

/**************************************************************************/
/* CAN's settings                                                         */
/**************************************************************************/


int tui_choiseCAN(void){
    return menu_choiser(can_faces, can_labels, CAN_SIZE);
}

int tui_choiseSpeed(void){
    return menu_choiser(speed_faces, speed_labels, SPEED_SIZE);
}

int menu_choiser(const int *faces, const char labels[][20], const int size){
    initscr();				/* start the curses mode */
    noecho();
    cbreak();
    curs_set( 0 );
    int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);
    //
    //mvprintw(0,0, "y = %d, x = %d", yMax, size);
    if(size > yMax) {
        printw("Bigger win");
        return 0;
    }
    else {
        WINDOW *menuwin = newwin(size + 2, xMax - 12, 2, 5);
        createWin(menuwin, "");

        int choice;
        int highlight = 0;

        while (1) {
            for (int i = 0; i < size; i++) {
                if (i == highlight)
                    wattron(menuwin, A_REVERSE);
                mvwprintw(menuwin, i + 1, 1, labels[i]);
                wattroff(menuwin, A_REVERSE);

            }
            choice = wgetch(menuwin);
            switch (choice) {
                case KEY_UP:
                    highlight = (!highlight) ? 0 : --highlight;
                    break;

                case KEY_DOWN:
                    highlight = (highlight == size - 1) ? size - 1 : ++highlight;
                    break;

                default:
                    break;
            }
            if (choice == 10)
                break;

        }
        //delwin( menuwin );
        clear();
        endwin();

        return faces[highlight];
    }
}